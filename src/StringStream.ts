interface ICursor{
    pos:number;
    ln:number;
    col:number;
}

class Cursor implements ICursor{
    public pos = 0;
    public ln  = 0;
    public col = 0;

    public constructor(){}

    public update( string?:string, length?:number ){
        for( var i=0; i<length; i++ ){
            var char = string.charAt(i);
            if (char !== '') {
                if ((char === '\n' && string.charAt( i + 1) === '\r') ||
                     char === '\n' ||
                     char === '\r') {
                    this.ln++;
                    this.col = 0;
                } else {
                    this.col++;
                }
            }
        }
        this.pos += length;
    }
}

class StringStream {
    protected _string;
    protected _source;
    protected _cursor:ICursor;

    public constructor(string){
        this._string = string;
        this._source = string;
        this._cursor = new Cursor();
    }

    public shift(n){
        (<Cursor>this._cursor).update(this._string,n);
        this._string = this._string.substr(n);
    }

    public toString(){
        return this._string;
    }

    public reset() {
        this._string = this._source;
        this._cursor.pos = 0;
        this._cursor.ln  = 0;
        this._cursor.col = 0;
    };

    get eof(){
        return this._string === '';
    }

    get cursor(){
        return this._cursor;
    }
}

export { ICursor, StringStream }
