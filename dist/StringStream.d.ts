interface ICursor {
    pos: number;
    ln: number;
    col: number;
}
declare class StringStream {
    protected _string: any;
    protected _source: any;
    protected _cursor: ICursor;
    constructor(string: any);
    shift(n: any): void;
    toString(): any;
    reset(): void;
    readonly eof: boolean;
    readonly cursor: ICursor;
}
export { ICursor, StringStream };
