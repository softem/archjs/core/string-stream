"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Cursor {
    constructor() {
        this.pos = 0;
        this.ln = 0;
        this.col = 0;
    }
    update(string, length) {
        for (var i = 0; i < length; i++) {
            var char = string.charAt(i);
            if (char !== '') {
                if ((char === '\n' && string.charAt(i + 1) === '\r') ||
                    char === '\n' ||
                    char === '\r') {
                    this.ln++;
                    this.col = 0;
                }
                else {
                    this.col++;
                }
            }
        }
        this.pos += length;
    }
}
class StringStream {
    constructor(string) {
        this._string = string;
        this._source = string;
        this._cursor = new Cursor();
    }
    shift(n) {
        this._cursor.update(this._string, n);
        this._string = this._string.substr(n);
    }
    toString() {
        return this._string;
    }
    reset() {
        this._string = this._source;
        this._cursor.pos = 0;
        this._cursor.ln = 0;
        this._cursor.col = 0;
    }
    ;
    get eof() {
        return this._string === '';
    }
    get cursor() {
        return this._cursor;
    }
}
exports.StringStream = StringStream;
//# sourceMappingURL=StringStream.js.map